# ZOT Assessment Test Manda
News API for ZOT assesment, [HTTP API URL](https://zot-assesment-test-manda.herokuapp.com/)

## Introduction

This HTTP API uses following tech stack: Postress, Heroku, TypeScript, Nest.js (with Fastify adapter), and Objectionjs ORM.

## Requirements

1. Postgress
2. Node.js (12.x.x)

## How to develop this HTTP API?

Step by step :

1. Fill the DATABASE_URL on your .env with your destination url
2. `npm install` install dependencies
3. `npm run migrate` migrate the database so you had initial table and column
4. `npm run seed` seeding the database with example value
4. `npm run start:dev`

## How to run it on production?

This HTTP API are deployed on Heroku, but if you want to deploy it on other platform the step by step are this

1. Fill the DATABASE_URL on your .env with your destination url
2. `npm install`
3. `npm run migrate`
4. `npm run build`
5. `npm run start:prod`

## How to run e2e test?

Run,

```bash
$ npm run start:dev
```

In other terminal instance

```bash
$ npm run test:e2e
```


## todo-list

1. Unit testing
2. Work on bonus points
3. Test mocking
4. Testing on CI/CD