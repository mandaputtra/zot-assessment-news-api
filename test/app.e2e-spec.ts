import { HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import * as Knex from 'knex';
import * as KnexConfig from '../knexfile';

describe('AppController (e2e)', () => {
  const app = 'http://localhost:3000';
  // TODO: User test database
  const knex = Knex(KnexConfig);

  const topic = { name: 'zen life' };
  const updatedTopic = { name: 'zen living' };
  let topicId = 0;

  const news = {
    title: '10 Kiat Sukses Seorang CEO',
    body: '1 - 10 Adalah bekerja keras',
    topics: ['culture', 'life', 'hobby'],
  };
  const updatedNews = {
    title: '10 Kiat Sukses Seorang PNS',
    body: '1 - 10 Adalah bekerja keras',
    topics: ['culture', 'life'],
  };
  let newsId = 0;

  beforeAll(async () => {
    await knex.migrate.rollback();
    await knex.migrate.latest();
  });

  afterAll(async () => {
    await knex.destroy();
  });

  it('/ (GET)', () => {
    return request(app).get('/').expect(200).expect({
      message:
        'read the docs on how to use this : https://documenter.getpostman.com/view/4153182/TzY68Z42',
    });
  });

  describe('CRUD on topics', () => {
    it('should create topic', async () => {
      await request(app)
        .post('/topics')
        .set('Accept', 'application/json')
        .send(topic)
        .expect(({ body }) => {
          expect(body.name).toBe(topic.name);
          expect(body.id).toBeDefined();
          // SET ID
          topicId = body.id;
        })
        .expect(HttpStatus.CREATED);
    });

    it('should yield error', async () => {
      await request(app)
        .post('/topics')
        .set('Accept', 'application/json')
        .send(topic)
        .expect(({ body }) => {
          expect(body.type).toBe('UniqueViolation');
          expect(body.data[0]).toBe('name');
        })
        .expect(HttpStatus.CONFLICT);
    });

    it('should get topic', async () => {
      await request(app)
        .get(`/topics/${topicId}`)
        .set('Accept', 'application/json')
        .send(topic)
        .expect(({ body }) => {
          expect(body.id).toBe(topicId);
          expect(body.name).toBe(topic.name);
        })
        .expect(HttpStatus.OK);
    });

    it('should list topics', async () => {
      await request(app)
        .get('/topics?page=1&itemsPerPage=10&sortBy=createdAt&descending=false')
        .set('Accept', 'application/json')
        .send(topic)
        .expect(({ body }) => {
          expect(body.results.length).toBe(body.total);
          expect(body.total).toBeDefined();
        })
        .expect(HttpStatus.OK);
    });

    it('should update topic', async () => {
      await request(app)
        .patch(`/topics/${topicId}`)
        .set('Accept', 'application/json')
        .send(updatedTopic)
        .expect(({ body }) => {
          expect(body.name).toBe(updatedTopic.name);
          expect(body.id).toBeDefined();
        })
        .expect(HttpStatus.OK);
    });

    it('should delete topic', async () => {
      await request(app)
        .delete(`/topics/${topicId}`)
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          expect(body.message).toBe('Success delete topics');
        })
        .expect(HttpStatus.OK);
    });
  });

  describe('CRUD on news', () => {
    it('should create new news', async () => {
      await request(app)
        .post(`/news`)
        .set('Accept', 'application/json')
        .send(news)
        .expect(({ body }) => {
          expect(body.title).toBe(news.title);
          expect(body.body).toBe(news.body);
          expect(body.topics.length).toBe(news.topics.length);
          newsId = body.id;
        })
        .expect(HttpStatus.CREATED);
    });

    it('should yield error', async () => {
      await request(app)
        .post('/news')
        .set('Accept', 'application/json')
        .send({ title: '' })
        .expect(({ body }) => {
          expect(body.type).toBe('HttpServerError');
          expect(body.data.message.length > 1).toBe(true);
        })
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should update news', async () => {
      await request(app)
        .patch(`/news/${newsId}`)
        .set('Accept', 'application/json')
        .send(updatedNews)
        .expect(({ body }) => {
          expect(body.title).toBe(updatedNews.title);
          expect(body.body).toBe(updatedNews.body);
          expect(body.topics.length).toBe(updatedNews.topics.length);
        })
        .expect(HttpStatus.OK);
    });

    it('search draft news', async () => {
      await request(app)
        .get('/news?status=DRAFT')
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          const news = body.results[0];
          expect(news.publishedAt).toBe(null);
          expect(news.deletedAt).toBe(null);
        })
        .expect(HttpStatus.OK);
    });

    it('search draft news with topics', async () => {
      await request(app)
        .get(`/news?status=DRAFT&topics=culture`)
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          const news = body.results[0];
          expect(news.topics).toBe('culture');
        })
        .expect(HttpStatus.OK);
    });

    it('should publish news', async () => {
      await request(app)
        .patch(`/news/publish/${newsId}`)
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          expect(body.message).toBe('Success publish news...');
          expect(body.data).toBe(1);
        })
        .expect(HttpStatus.OK);
    });

    it('list publised news', async () => {
      await request(app)
        .get('/news')
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          const news = body.results[0];
          expect(news.publishedAt !== null).toBe(true);
          expect(news.deletedAt).toBe(null);
        })
        .expect(HttpStatus.OK);
    });

    it('should delete news', async () => {
      await request(app)
        .delete(`/news/${newsId}`)
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          expect(body.message).toBe('Success delete news...');
          expect(body.data).toBe(1);
        })
        .expect(HttpStatus.OK);
    });

    it('list deleted news', async () => {
      await request(app)
        .get('/news?status=DELETE')
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          const news = body.results[0];
          expect(news.deletedAt !== null).toBe(true);
        })
        .expect(HttpStatus.OK);
    });

    it('should get news', async () => {
      await request(app)
        .get(`/news/${newsId}`)
        .set('Accept', 'application/json')
        .expect(({ body }) => {
          expect(body.title).toBe(updatedNews.title);
          expect(body.body).toBe(updatedNews.body);
          expect(body.topics.length > 1).toBe(true);
          expect(body.publishedAt !== null).toBe(true);
          expect(body.deletedAt !== null).toBe(true);
          expect(body.createdAt !== null).toBe(true);
        })
        .expect(HttpStatus.OK);
    });
  });
});
