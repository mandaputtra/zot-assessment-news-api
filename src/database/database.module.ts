import { Global, Module } from '@nestjs/common';
import { Model, knexSnakeCaseMappers } from 'objection';
import * as Knex from 'knex';

// model
import { NewsTopicsModel } from './models/news-topics.model';
import { NewsModel } from './models/news.model';
import { TopicsModel } from './models/topics.model';

const models = [NewsTopicsModel, NewsModel, TopicsModel];

const modelProviders = models.map((model) => {
  return {
    provide: model.name,
    useValue: model,
  };
});

const providers = [
  ...modelProviders,
  {
    provide: 'KnexConnection',
    useFactory: async () => {
      const knex = Knex({
        client: 'pg',
        connection: process.env.DATABASE_URL,
        debug: process.env.KNEX_DEBUG === 'true',
        ...knexSnakeCaseMappers(),
      });

      Model.knex(knex);
      return knex;
    },
  },
];

@Global()
@Module({
  providers: [...providers],
  exports: [...providers],
})
export class DatabaseModule {}
