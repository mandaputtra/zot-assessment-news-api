import * as Knex from 'knex';
import * as dayjs from 'dayjs';
import { NewsTopicsModel } from '../models/news-topics.model';
import { TopicsModel } from '../models/topics.model';
import { NewsModel } from '../models/news.model';

export async function seed(knex: Knex): Promise<any> {
  const LOREM_IPSUM =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

  const topics = await TopicsModel.query(knex).insert([
    { name: 'politics' },
    { name: 'economy' },
    { name: 'auto' },
    { name: 'education' },
    { name: 'tech' },
    { name: 'culture' },
    { name: 'money' },
    { name: 'sports' },
    { name: 'uncategorized' },
  ]);

  const news = await NewsModel.query(knex).insert([
    {
      title: 'Tax are Increasing Again, But Why?',
      body: LOREM_IPSUM,
    },
    {
      title: 'AwanKinton are Making Brand New University Named DragonBall',
      body: LOREM_IPSUM,
    },
    {
      title: 'AwanKinton Released New Car that Can Fly',
      body: LOREM_IPSUM,
    },
    {
      title: 'Indonesia National Team Are Not Gonna Make it to World Cup',
      body: LOREM_IPSUM,
      publishedAt: dayjs().toISOString(),
    },
  ]);

  await NewsTopicsModel.query(knex).insert([
    { newsId: news[0].id, topicsId: topics[6].id },
    { newsId: news[0].id, topicsId: topics[3].id },
    { newsId: news[1].id, topicsId: topics[2].id },
    { newsId: news[1].id, topicsId: topics[3].id },
    { newsId: news[1].id, topicsId: topics[4].id },
    { newsId: news[2].id, topicsId: topics[2].id },
    { newsId: news[2].id, topicsId: topics[3].id },
    { newsId: news[3].id, topicsId: topics[7].id },
  ]);
}
