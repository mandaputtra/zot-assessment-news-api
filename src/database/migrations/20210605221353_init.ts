import * as Knex from 'knex';
import { News, NewsTopics, Topics } from '../table-name';

export async function up(knex: Knex) {
  return knex.schema
    .createTable(News, (t) => {
      // this creates an "id" column that gets autoincremented
      t.increments();
      t.string('title').notNullable();
      t.text('body').notNullable();
      t.timestamp('published_at');
      t.timestamp('deleted_at');
      t.timestamp('created_at').defaultTo(knex.fn.now());
    })
    .createTable(Topics, (t) => {
      t.increments();
      t.string('name').notNullable().unique();
      t.timestamp('created_at').defaultTo(knex.fn.now());
    })
    .createTable(NewsTopics, (t) => {
      t.increments();
      t.integer('news_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(News)
        .onDelete('CASCADE')
        .index();
      t.integer('topics_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(Topics)
        .onDelete('CASCADE')
        .index();
    });
}

export async function down(knex: Knex) {
  return knex.schema
    .dropTableIfExists(NewsTopics)
    .dropTableIfExists(Topics)
    .dropTableIfExists(News);
}
