import { BaseModel } from '../base.model';
import { News, NewsTopics, Topics } from '../table-name';
import { Model } from 'objection';
import { NewsModel } from './news.model';
import { TopicsModel } from './topics.model';
import { join } from 'path';
export class NewsTopicsModel extends BaseModel {
  static tableName = NewsTopics;

  newsId: number;
  topicsId: number;

  news: NewsModel;
  topics: TopicsModel;

  static relationMappings = {
    topics: {
      modelClass: join(__dirname, 'topics.model'),
      relation: Model.BelongsToOneRelation,
      join: {
        from: `${NewsTopics}.topics_id`,
        to: `${Topics}.id`,
      },
    },
    news: {
      modelClass: join(__dirname, 'news.model'),
      relation: Model.BelongsToOneRelation,
      join: {
        from: `${NewsTopics}.news_id`,
        to: `${News}.id`,
      },
    },
  };
}
