import { BaseModel } from '../base.model';
import { News, NewsTopics, Topics } from '../table-name';
import { Model } from 'objection';
import { NewsModel } from './news.model';
import { join } from 'path';
export class TopicsModel extends BaseModel {
  static tableName = Topics;

  name: string;
  createdAt: string;

  news: NewsModel[];

  static relationMappings = {
    news: {
      modelClass: join(__dirname, 'news.model'),
      relation: Model.ManyToManyRelation,
      join: {
        from: `${Topics}.id`,
        through: {
          // persons_movies is the join table.
          from: `${NewsTopics}.topics_id`,
          to: `${NewsTopics}.news_id`,
        },
        to: `${News}.id`,
      },
    },
  };
}
