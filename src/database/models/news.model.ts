import { BaseModel } from '../base.model';
import { News, NewsTopics, Topics } from '../table-name';
import { Model } from 'objection';
import { TopicsModel } from './topics.model';
import { join } from 'path';
export class NewsModel extends BaseModel {
  static tableName = News;

  title: string;
  body: string;
  createdAt: string;
  publishedAt: string;
  deletedAt: string;

  topics: TopicsModel;

  static relationMappings = {
    topics: {
      modelClass: join(__dirname, 'topics.model'),
      relation: Model.ManyToManyRelation,
      join: {
        from: `${News}.id`,
        through: {
          // persons_movies is the join table.
          from: `${NewsTopics}.news_id`,
          to: `${NewsTopics}.topics_id`,
        },
        to: `${Topics}.id`,
      },
    },
  };
}
