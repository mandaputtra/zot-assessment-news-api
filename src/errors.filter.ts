import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';

import {
  ValidationError,
  NotFoundError,
  DBError,
  ConstraintViolationError,
  UniqueViolationError,
  NotNullViolationError,
  ForeignKeyViolationError,
  CheckViolationError,
  DataError,
} from 'objection';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger = new Logger(AllExceptionsFilter.name);

  catch(exception: InternalServerErrorException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const DefaultStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    this.logger.error(exception, 'CATCH:ERROR:FILTER');
    if (exception instanceof ValidationError) {
      switch (exception.type) {
        case 'ModelValidation':
          response.code(HttpStatus.BAD_REQUEST).send({
            message: exception.message,
            type: exception.type,
            data: exception.data,
            timestamp: new Date().toISOString(),
            statusCode: HttpStatus.BAD_REQUEST,
            path: request.url,
          });
          break;
        case 'RelationExpression':
          response.code(HttpStatus.BAD_REQUEST).send({
            message: exception.message,
            type: 'RelationExpression',
            data: {},
            timestamp: new Date().toISOString(),
            statusCode: HttpStatus.BAD_REQUEST,
            path: request.url,
          });
          break;
        case 'UnallowedRelation':
          response.code(HttpStatus.BAD_REQUEST).send({
            message: exception.message,
            type: exception.type,
            data: {},
            timestamp: new Date().toISOString(),
            statusCode: HttpStatus.BAD_REQUEST,
            path: request.url,
          });
          break;
        case 'InvalidGraph':
          response.code(HttpStatus.BAD_REQUEST).send({
            message: exception.message,
            type: exception.type,
            data: {},
            timestamp: new Date().toISOString(),
            statusCode: HttpStatus.BAD_REQUEST,
            path: request.url,
          });
          break;
        default:
          response.code(HttpStatus.BAD_REQUEST).send({
            message: exception.message,
            type: 'UnknownValidationError',
            data: {},
            timestamp: new Date().toISOString(),
            statusCode: HttpStatus.BAD_REQUEST,
            path: request.url,
          });
          break;
      }
    } else if (exception instanceof UniqueViolationError) {
      response.code(HttpStatus.CONFLICT).send({
        statusCode: HttpStatus.CONFLICT,
        data: exception.columns,
        message: exception.message,
        timestamp: new Date().toISOString(),
        type: 'UniqueViolation',
        path: request.url,
      });
    } else if (exception instanceof DBError) {
      response.code(HttpStatus.INTERNAL_SERVER_ERROR).send({
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'UnknownDBError',
        path: request.url,
      });
    } else if (exception instanceof DataError) {
      response.code(HttpStatus.BAD_REQUEST).send({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'InvalidData',
        path: request.url,
      });
    } else if (exception instanceof CheckViolationError) {
      response.code(HttpStatus.BAD_REQUEST).send({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'CheckViolation',
        path: request.url,
      });
    } else if (exception instanceof ForeignKeyViolationError) {
      response.code(HttpStatus.BAD_REQUEST).send({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'ForeignKeyViolation',
        path: request.url,
      });
    } else if (exception instanceof NotNullViolationError) {
      response.code(HttpStatus.BAD_REQUEST).send({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'NotNullViolation',
        path: request.url,
      });
    } else if (exception instanceof ConstraintViolationError) {
      response.code(HttpStatus.BAD_REQUEST).send({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'ConstraintViolation',
        path: request.url,
      });
    } else if (exception instanceof NotFoundError) {
      response.code(HttpStatus.NOT_FOUND).send({
        statusCode: HttpStatus.NOT_FOUND,
        timestamp: new Date().toISOString(),
        message: exception.message,
        type: 'NotFound',
        path: request.url,
      });
    } else if (exception instanceof HttpException) {
      response.code(DefaultStatus).send({
        message: exception.message,
        data: exception.getResponse(),
        statusCode: DefaultStatus,
        type: 'HttpServerError',
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    } else {
      // TODO: Logger maybe
      console.error(exception);
      response.code(DefaultStatus).send({
        type: 'UnknownErrorServer',
        statusCode: DefaultStatus,
        message: exception,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    }
  }
}
