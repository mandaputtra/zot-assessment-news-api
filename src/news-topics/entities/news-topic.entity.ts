import { ModelObject } from 'objection';
import { NewsTopicsModel } from '../../database/models/news-topics.model';
import { IsNumber } from 'class-validator';

export type NewsTopicsEntity = ModelObject<NewsTopicsModel>;

export class NewsTopicsDto {
  @IsNumber()
  newsId: number;

  @IsNumber()
  topicsId: number;
}
