import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return read the docs', () => {
      expect(appController.getHello()).toMatchObject({
        message:
          'read the docs on how to use this : https://documenter.getpostman.com/view/4153182/TzY68Z42',
      });
    });
  });
});
