import { ModelObject } from 'objection';
import { TopicsModel } from '../../database/models/topics.model';

export type TopicsEntity = ModelObject<TopicsModel>;
