import {
  IsBooleanString,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class QueryTopicDto {
  @IsOptional()
  search?: string;

  @IsOptional()
  @IsNumberString()
  page = '1';

  @IsOptional()
  @IsNumberString()
  itemsPerPage = '10';

  @IsOptional()
  @IsString()
  sortBy = 'createdAt';

  @IsOptional()
  @IsBooleanString()
  descending = 'false';
}
