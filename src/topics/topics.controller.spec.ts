import { Test, TestingModule } from '@nestjs/testing';
import { TopicsModel } from '../database/models/topics.model';
import { TopicsController } from './topics.controller';
import { TopicsRepository } from './topics.repository';
import { TopicsService } from './topics.service';

describe('TopicsService', () => {
  let controller: TopicsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TopicsController],
      providers: [TopicsService, TopicsRepository, TopicsModel],
    }).compile();

    controller = module.get<TopicsController>(TopicsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
