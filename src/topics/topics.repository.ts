import { Inject, Injectable } from '@nestjs/common';
import { ModelClass, OrderByDirection, Page } from 'objection';
import { TopicsModel } from 'src/database/models/topics.model';
import { CreateTopicDto } from './dto/create-topic.dto';
import { QueryTopicDto } from './dto/query-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { TopicsEntity } from './entities/topic.entity';

@Injectable()
export class TopicsRepository {
  constructor(
    @Inject('TopicsModel') private topicsModel: ModelClass<TopicsModel>,
  ) {}

  async create(body: CreateTopicDto): Promise<TopicsEntity> {
    const topic = await this.topicsModel.query().insert(body);
    return topic;
  }

  async findAll(query: QueryTopicDto): Promise<Page<TopicsModel>> {
    const { itemsPerPage, page, sortBy, descending, search } = query;
    const model = this.topicsModel.query().select();
    if (search) {
      model.whereRaw(`LOWER(name) LIKE ?`, [`%${search}%`]);
    }
    const offset: number = +page === 1 ? 0 : +itemsPerPage * +page;
    const orderAs: OrderByDirection = descending ? 'asc' : 'desc';
    model.orderBy(sortBy, orderAs);
    const topics = await model.page(offset, +itemsPerPage);
    return topics;
  }

  async findOne(id: number): Promise<TopicsEntity> {
    const topic = await this.topicsModel.query().findById(id);
    return topic;
  }

  async update(id: number, body: UpdateTopicDto): Promise<TopicsEntity> {
    const topic = await this.topicsModel
      .query()
      .findById(id)
      .patch(body)
      .returning('*')
      .first();
    return topic;
  }

  async remove(id: number): Promise<number> {
    const topic = await this.topicsModel.query().findById(id).delete();
    return topic;
  }
}
