import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { TopicsService } from './topics.service';
import { CreateTopicDto } from './dto/create-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { QueryTopicDto } from './dto/query-topic.dto';

@Controller('topics')
export class TopicsController {
  constructor(private readonly topicsService: TopicsService) {}

  @Post()
  async create(@Body() createTopicDto: CreateTopicDto) {
    try {
      const data = await this.topicsService.create(createTopicDto);
      return data;
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll(@Query() query: QueryTopicDto) {
    try {
      const data = await this.topicsService.findAll(query);
      return data;
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const data = await this.topicsService.findOne(+id);
      return data;
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateTopicDto: UpdateTopicDto,
  ) {
    try {
      const data = await this.topicsService.update(+id, updateTopicDto);
      return data;
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      await this.topicsService.remove(+id);
      return { message: 'Success delete topics' };
    } catch (error) {
      throw error;
    }
  }
}
