import { Injectable } from '@nestjs/common';
import { Page } from 'objection';
import { TopicsModel } from 'src/database/models/topics.model';
import { CreateTopicDto } from './dto/create-topic.dto';
import { QueryTopicDto } from './dto/query-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { TopicsEntity } from './entities/topic.entity';
import { TopicsRepository } from './topics.repository';

@Injectable()
export class TopicsService {
  constructor(private topicsRepository: TopicsRepository) {}

  async create(body: CreateTopicDto): Promise<TopicsEntity> {
    return await this.topicsRepository.create(body);
  }

  async findAll(query: QueryTopicDto): Promise<Page<TopicsModel>> {
    return await this.topicsRepository.findAll(query);
  }

  async findOne(id: number): Promise<TopicsEntity> {
    return await this.topicsRepository.findOne(id);
  }

  async update(id: number, body: UpdateTopicDto): Promise<TopicsEntity> {
    return await this.topicsRepository.update(id, body);
  }

  async remove(id: number): Promise<number> {
    return await this.topicsRepository.remove(id);
  }
}
