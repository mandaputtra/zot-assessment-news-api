import { Test, TestingModule } from '@nestjs/testing';
import { NewsTopicsModel } from '../database/models/news-topics.model';
import { NewsModel } from '../database/models/news.model';
import { TopicsModel } from '../database/models/topics.model';
import { NewsController } from './news.controller';
import { NewsRepository } from './news.repository';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [
        NewsService,
        NewsRepository,
        NewsModel,
        TopicsModel,
        NewsTopicsModel,
      ],
    }).compile();

    controller = module.get<NewsController>(NewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
