import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { NewsRepository } from './news.repository';

@Module({
  controllers: [NewsController],
  providers: [NewsService, NewsRepository],
  exports: [NewsRepository, NewsService],
})
export class NewsModule {}
