import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsDto, NewsStatus } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { QueryNewsDto } from './dto/query-news.dto';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Post()
  async create(@Body() body: CreateNewsDto) {
    if (body.status === NewsStatus.DELETE) {
      throw new HttpException(
        "Can't delete news when you just create it",
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      return await this.newsService.create(body);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll(@Query() query: QueryNewsDto) {
    try {
      return await this.newsService.findAll(query);
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const news = await this.newsService.findOne(+id);
      return news;
    } catch (error) {
      throw error;
    }
  }

  @Patch('publish/:id')
  async publish(@Param('id') id: string) {
    try {
      const data = await this.newsService.publishNews(+id);
      if (data) {
        return { message: 'Success publish news...', data };
      } else {
        throw new HttpException('Not Found', HttpStatus.BAD_REQUEST);
      }
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body: UpdateNewsDto) {
    if (body.status === NewsStatus.DELETE) {
      throw new HttpException(
        "Can't delete news when you just update it",
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      const data = await this.newsService.update(+id, body);
      if (data) {
        return data;
      } else {
        throw new HttpException('Not Found', HttpStatus.BAD_REQUEST);
      }
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const data = await this.newsService.remove(+id);

      if (data) {
        return { message: 'Success delete news...', data };
      } else {
        throw new HttpException('Not Found', HttpStatus.BAD_REQUEST);
      }
    } catch (error) {
      throw error;
    }
  }
}
