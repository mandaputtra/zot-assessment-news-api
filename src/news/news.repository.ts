import { Injectable, Inject } from '@nestjs/common';
import { ModelClass, OrderByDirection, Transaction } from 'objection';
import { NewsModel } from '../database/models/news.model';
import { TopicsModel } from '../database/models/topics.model';
import { NewsTopicsModel } from '../database/models/news-topics.model';
import { CreateTopicDto } from '../topics/dto/create-topic.dto';
import { CreateNewsDto, NewsStatus } from './dto/create-news.dto';
import { NewsTopicsDto } from 'src/news-topics/entities/news-topic.entity';
import * as dayjs from 'dayjs';
import { UpdateNewsDto } from './dto/update-news.dto';
import { QueryNewsDto } from './dto/query-news.dto';
import { NewsEntity } from './entities/news.entity';

@Injectable()
export class NewsRepository {
  constructor(
    @Inject('NewsModel') private newsModel: ModelClass<NewsModel>,
    @Inject('TopicsModel') private topicsModel: ModelClass<TopicsModel>,
    @Inject('NewsTopicsModel')
    private newsTopicsModel: ModelClass<NewsTopicsModel>,
  ) {}

  async create(body: CreateNewsDto): Promise<NewsEntity> {
    const news = await this.newsModel.transaction(async (trx) => {
      const newNews = await this.newsModel.query(trx).insert({
        title: body.title,
        body: body.body,
      });
      await this.insertTopicsOnNews(body.topics, newNews.id, trx);
      // Set status of news
      if (body.status === NewsStatus.PUBLISH) {
        await this.newsModel
          .query(trx)
          .findById(newNews.id)
          .patch({ publishedAt: dayjs().toISOString() });
      }
      // Fetch new news with topics
      const news = await this.newsModel
        .query(trx)
        .select('news.*')
        .findById(newNews.id)
        .withGraphFetched('topics');
      return news;
    });
    return news;
  }

  async update(id: number, body: UpdateNewsDto): Promise<NewsEntity> {
    const news = this.newsModel.transaction(async (trx) => {
      await this.newsModel
        .query(trx)
        .findById(id)
        .patch({ body: body.body, title: body.title });
      // Delete all topics on related one
      await this.newsTopicsModel.query(trx).where('newsId', id).delete();
      await this.insertTopicsOnNews(body.topics, id, trx);
      // Fetch new news with topics
      const news = await this.newsModel
        .query(trx)
        .select('news.*')
        .findById(id)
        .withGraphFetched('topics');
      return news;
    });
    return news;
  }

  /**  Check if the topics find length and inserted one are different.
  If the topics length different there must be new topics created,
  and then insert in on news_topics. Assign uncategorized if news doesn't have topics
  */
  async insertTopicsOnNews(
    insertedTopics: string[],
    id: number,
    trx: Transaction,
  ): Promise<void> {
    const lowerCaseTopics = insertedTopics.map((tp) => tp.toLowerCase());
    // Assign uncategorized
    if (lowerCaseTopics.length < 1) {
      lowerCaseTopics.push('uncategorized');
    }

    // Insert the topics again
    const topics = await this.topicsModel
      .query(trx)
      .whereIn('name', lowerCaseTopics);

    let createdTopics: TopicsModel[] = [];
    if (lowerCaseTopics.length !== topics.length) {
      const newTopics: CreateTopicDto[] = [];
      for (let i = 0; i < lowerCaseTopics.length; i++) {
        const topic = lowerCaseTopics[i];
        const searchIndex = topics.findIndex((tp) => tp.name === topic);
        if (searchIndex === -1) {
          newTopics.push({ name: topic });
        }
      }
      // Create new topics
      createdTopics = await this.topicsModel.query(trx).insert(newTopics);
      // Merge new array of topics
    }
    const newsTopics: NewsTopicsDto[] = [...topics, ...createdTopics].map(
      (tp) => ({
        newsId: id,
        topicsId: tp.id,
      }),
    );
    await this.newsTopicsModel.query(trx).insert(newsTopics);
  }

  async findAll(query: QueryNewsDto): Promise<any> {
    const { itemsPerPage, page, sortBy, descending, search, topics, status } =
      query;

    const model = this.newsModel.query();
    if (search) {
      model.whereRaw(`LOWER(title) LIKE ?`, [`%${search.toLowerCase()}%`]);
    }

    if (status === NewsStatus.DELETE) {
      model.whereRaw('deleted_at IS NOT NULL');
    } else if (status === NewsStatus.DRAFT) {
      model.whereRaw('published_at IS NULL AND deleted_at IS NULL');
    } else {
      model.whereRaw('published_at IS NOT NULL AND deleted_at IS NULL');
    }

    if (topics) {
      model.whereIn('topics.name', topics.split(','));
    }

    const offset: number = +page === 1 ? 0 : +itemsPerPage * +page;
    const orderAs: OrderByDirection = descending ? 'asc' : 'desc';
    model.orderBy(sortBy, orderAs);
    model.withGraphJoined('topics').debug();
    const news = await model.page(offset, +itemsPerPage);
    return news;
  }

  async findOne(id: number): Promise<NewsEntity> {
    const news = await this.newsModel
      .query()
      .select('news.*')
      .findById(id)
      .withGraphFetched('topics');
    return news;
  }

  async publishNews(id: number): Promise<number> {
    const publish = await this.newsModel
      .query()
      .findById(id)
      .patch({ publishedAt: dayjs().toISOString() });
    return publish;
  }

  async deleteNews(id: number): Promise<number> {
    const deleteNews = await this.newsModel
      .query()
      .findById(id)
      .patch({ deletedAt: dayjs().toISOString() });
    return deleteNews;
  }
}
