import {
  IsBooleanString,
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { NewsStatus } from './create-news.dto';

export class QueryNewsDto {
  @IsOptional()
  search?: string;

  @IsOptional()
  @IsEnum(NewsStatus)
  status?: NewsStatus = NewsStatus.PUBLISH;

  @IsOptional()
  topics: string;

  @IsOptional()
  @IsNumberString()
  page = '1';

  @IsOptional()
  @IsNumberString()
  itemsPerPage = '10';

  @IsOptional()
  @IsString()
  sortBy = 'createdAt';

  @IsOptional()
  @IsBooleanString()
  descending = 'false';
}
