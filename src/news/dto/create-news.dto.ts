import { IsEnum, IsString, MinLength } from 'class-validator';

export enum NewsStatus {
  DRAFT = 'DRAFT',
  PUBLISH = 'PUBLISH',
  DELETE = 'DELETE',
}
export class CreateNewsDto {
  @IsString()
  @MinLength(5)
  title: string;

  @IsString()
  @MinLength(10)
  body: string;

  @IsString({ each: true })
  topics: string[] = ['uncategorized'];

  @IsEnum(NewsStatus)
  status: NewsStatus = NewsStatus.DRAFT;
}
