import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { QueryNewsDto } from './dto/query-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { NewsEntity } from './entities/news.entity';
import { NewsRepository } from './news.repository';

@Injectable()
export class NewsService {
  constructor(private newsRepository: NewsRepository) {}

  async create(body: CreateNewsDto): Promise<NewsEntity> {
    const news = await this.newsRepository.create(body);
    return news;
  }

  async publishNews(id: number): Promise<number> {
    return this.newsRepository.publishNews(id);
  }

  async findAll(query: QueryNewsDto): Promise<any> {
    return this.newsRepository.findAll(query);
  }

  async findOne(id: number): Promise<NewsEntity> {
    const news = await this.newsRepository.findOne(+id);
    return news;
  }

  async update(id: number, body: UpdateNewsDto): Promise<NewsEntity> {
    const news = await this.newsRepository.update(id, body);
    return news;
  }

  async remove(id: number): Promise<number> {
    return await this.newsRepository.deleteNews(id);
  }
}
