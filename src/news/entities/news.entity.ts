import { ModelObject } from 'objection';
import { NewsModel } from '../../database/models/news.model';

export type NewsEntity = ModelObject<NewsModel>;

export interface NewsTopicEntity extends NewsEntity {
  topic: string;
}

export interface NewsTopicsEntity extends NewsEntity {
  topics_name: string;
}
