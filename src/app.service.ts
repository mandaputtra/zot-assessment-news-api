import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): { message: string } {
    return {
      message:
        'read the docs on how to use this : https://documenter.getpostman.com/view/4153182/TzY68Z42',
    };
  }
}
