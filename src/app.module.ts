import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './news/news.module';
import { TopicsModule } from './topics/topics.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [DatabaseModule, NewsModule, TopicsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
